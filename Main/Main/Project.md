[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)


# Project Idea: Smart cleaner

![](./Images/tech.2.jpeg)

## Overview
The Smart Floor Cleaner Robot is an autonomous cleaning robot designed to navigate indoor environments, detect obstacles, and effectively clean floors. It incorporates various sensors, including ultrasonic sensors for obstacle detection and a DHT11 sensor for environmental monitoring. The robot is controlled by an Arduino Uno R3 microcontroller and utilizes a motor driver board to drive the movement of wheels and activate a vacuum cleaner mechanism. Additionally, an OLED display provides real-time feedback to the user, while LEDs serve as visual indicators for different states of operation.

## Features
#### Obstacle Detection and Navigation:
 The robot uses ultrasonic sensors to detect obstacles in its path and navigates around them to ensure uninterrupted cleaning.
#### Environmental Monitoring: 
The DHT11 sensor measures temperature and humidity levels in the environment, providing insights for efficient cleaning and user comfort.
#### User Interaction: 
A push button allows users to initiate or terminate the cleaning process, providing ease of control.
#### Efficient Cleaning Mechanism: 
A DC motor powers the vacuum cleaner mechanism, effectively sucking up dust and debris from the floor surface for thorough cleaning.
#### Mop Attachment (Optional): 
An additional feature allows users to attach a mop for mopping hard floors, enhancing the robot's versatility.
#### Status Display: 
An OLED display provides real-time status updates, including cleaning progress, battery level, and environmental conditions, ensuring user awareness throughout the cleaning process.
#### Power Management: 
The robot operates on a rechargeable lithium battery, ensuring convenient and cordless operation. The Arduino monitors the battery level and displays a warning when it requires recharging.
#### Customizable Design: 
The project offers flexibility for customization and expansion, allowing enthusiasts to add additional features or modify existing ones according to their preferences and requirements.

## Objective
The Smart Floor Cleaner Robot project aims to create a robot that cleans floors automatically. It uses sensors to avoid obstacles and monitors the room's temperature. With a push of a button, users can start or stop the cleaning process. The robot sucks up dirt using a vacuum and can also mop floors if needed. It shows information on a small screen and runs on a rechargeable battery, making it easy to use and convenient for keeping floors clean.

## Working concept

![](./Images/tech.1.jpeg)

The smart cleaner robot uses sensors and motors to move around and clean floors. It has an ultrasonic sensor to detect obstacles, like furniture or walls, so it doesn't bump into them. There's also a sensor to check the temperature and humidity in the room. When you press a button, the robot starts cleaning. It moves around, avoiding obstacles, while a motor turns on to suck up dust and dirt from the floor. If you attach a mop, it can also mop the floor. The robot shows information on a small screen, like if it's cleaning or if the battery is low. It runs on a rechargeable battery, so you don't need to plug it in while it cleans. Overall, it's a smart and handy way to keep your floors clean!

![](./Images/tech.jpeg)

### Components required
1. Arduino uno r3
2. Breadboard
3. Ultrasonic sensor
4. DHT11
5. Motor Driver board
6. Dc Motor
7. Wire
8. Push Butoon
9. Oled
10. Leds
11. Wheels
12. Propeller
13. Resistor
14. Lithium Battery
15. Mop

[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)
