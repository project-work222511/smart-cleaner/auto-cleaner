[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads) | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)
### Designs
We used tinkercad to design the parts required for designing auto cleaner
![](./Images/circuit.png)
### Code for the above circuit
// Define the pins
const int motorPin = 3;   // Pin connected to the gate of the NMOS transistor
const int buttonPin = 2;  // Pin connected to the push button

void setup() {
  // Initialize the motor pin as an output
  pinMode(motorPin, OUTPUT);
  // Initialize the button pin as an input
  pinMode(buttonPin, INPUT);
}

void loop() {
  // Read the state of the push button
  int buttonState = digitalRead(buttonPin);

  // If the button is pressed, turn on the motor
  if (buttonState == HIGH) {
    digitalWrite(motorPin, HIGH); // Turn on the motor
  } else {
    digitalWrite(motorPin, LOW);  // Turn off the motor
  }
}


![](./Images/progress.png)
##### Circuit design of controlling motor with PWM

![](./Images/top.png)
#### Design of the top part of auto cleaner

![](./Images/buttom.png)
#### design of the buttom part of auto cleaner

![](./Images/vacuum.png)
#### design of the vacuum holder

[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads) | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)

#### Assembling Materials required
#### Materials assembled: 
1. Power bank
2. Jumper wires
3. Motor driver
4. Arduino uno 3
5. Ultrasonic sensor
6. DC motor
![](./Images/start.png)
 
 #### Wheel
 ![](./Images/ardu.jpg)

 
 We used inkscape to design a circle as our wheel and then used laser cutter to make  4 wheels out of plywood.
 We have 4 motors attached to 4 plywood wheels. We used 4 motors so we can upload a code to specify which motor to turn when we need to turn the auto cleaner.
 We attached the 4 motor wheels on a plywood board and connected each of the motor to arduino uno. Motor 1 is connected to M1, motor 2 is connected to M2 and same for the other two motors.

 #### vacuum cleaner
 ![](./Images/cup.jpg)
 We are planning to use plastic cup as a medium to suck in dust and wastes using dc motor. Basically we are planning to make vacuum cleaner using plastic cup and dc motor. 

