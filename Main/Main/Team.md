[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)

# Team Members

1. Dhan Bdr. Rana Mongar

![](./Images/Rana.jpg)

- Age: 17
- Hobbies: Gaming, Sports, and rading
- What I hope to learn: I'm really excited to learn more about Arduino programming and how to use technology in 
  environmental projects. Also, I want to get better at working in a team and talking with others.
- Contribution to the team: I'm good at finding creative solutions to problems and paying attention to small details. I'll 
  work on making our smart cleaner look nice and work well.

2. Pema Ugyen Dendup

![](./Images/Pema.jpg)

- Age: 17
- Hobbies: Playing games and sports, and solving Math problems
- What I hope to learn: I hope to learn about new components, how we can use those componenets to make something, and working together in team.
- Contribution to the team: I do documentation for the team.

3. Rinchen Namgay

![](./Images/Rinchen.jpg)

- Age: 17
- Hobbies: My interest lies in the field of basketball and and reading(quite often).
- What I hope to learn: From this project I wish and hope to learn how sensors function, do some codings, and overall I want to learn how our product functions.
- Contribution to the team: I helped my peers in documentating our project, motivated them in any way I could, and I also helped in sketching our works.

4. Purna Bahadur Subedi

![](./Images/Purna.jpg)

- Age: 17
- Hobbies: Playng chess, reading, and learning almost every kind of genre(general knowledge).
- What I hope to learn: I want to learn about working of arduino, and the process of building arduino based project(e.g: coding, wiring).
- Contributions to the team: I am currently doing the planning of our overall project and sketching of the project. I want to make sure that every member of my team are learning equally and contributing to the group.

5. Sonam Wangchuk

![](./Images/Sonam.jpg)

- Age: 17
- Hobbies: I like playing  basketball, football , table tennis, fotsal and volleyball. I also love to do art works and learning about technologies. 
- What I hope to learn: I want to learn about arduino and other electronic components. 
- Contributions to the team: I am learning online and doing the sketch and doing research on how we can make our model efficent. 


[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)




