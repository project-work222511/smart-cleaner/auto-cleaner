[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)

# Explanation for the Components that we learned

#### Arduino Uno R3
Used for the codeing and overall designing

![](./Images/arduino-uno-r3-board.jpeg)

#### Ultrasonic Sensor: 
Used for obstacle detection and navigation.

![](./Images/ultra.jpg)


#### DHT11: 
This sensor can measure temperature and humidity, which could be useful for monitoring the environment while cleaning.

![](./Images/dht11.webp)

#### Motor Driver Board: 
Necessary for controlling the DC motors used for movement and the vacuum cleaner mechanism.

![](./Images/2_2_1.jpeg)


#### DC Motor: 
Powers the wheels for movement and potentially drives the vacuum cleaner mechanism.

![](./Images/dc.jpeg)

#### Push Button: 
A user input component that could be used for starting/stopping the cleaning process or triggering other actions.

![](./Images/push.webp)

#### OLED Display: 
Provides feedback to the user, such as status updates or sensor readings.

![](./Images/oled.jpeg)

#### LEDs: 
Visual indicators for various states of the robot, such as power status or cleaning mode.

![](./Images/leds.jpeg)

#### Wheels: 
Essential for movement.

![](./Images/wheels.webp)

#### Propeller: 
Depending on the design, this could potentially be used for additional propulsion or as part of the vacuum mechanism.

![](./Images/propeller.jpeg)

#### Resistor: 
Used to limit current or adjust voltage levels as needed.

![](./Images/resistor.jpeg)

#### Lithium Battery: 
Provides power for the robot's operation, ensuring it can move freely without being tethered to a power source.

![](./Images/Lithium.webp)


[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)


