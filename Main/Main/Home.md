[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)


# Hygiene Innovators

Welcome to our homepage of "Hygiene Innovators"! We're a group of five grade 11 students dedicated to keeping our classroom clean and hygienic. Our team members—Pema Ugyen Dendup, Rinchen Namgay, Dhan Bdr. Rana Mongar, Purna Bahadur Subedi, and Sonam Wangchuk—each contribute unique skills and ideas.

We've noticed that our classroom often lacks proper cleaning, so we've come together to address this issue by designing a smart cleaner. Our goal is to create a device that can efficiently clean the classroom without much manual effort. With our combined talents and creativity, we're excited to make a difference in our school environment and promote better hygiene practices for everyone. 
![](./Images/group.jpg)

## Our project
![](./Images/tech.2.jpeg)

Our project, the " Smart Cleaner," is an innovative solution aimed at revolutionizing classroom hygiene. By combining Arduino technology with components like ultrasonic sensors, DC motors, and humidity sensors, our cleaner offers automated, efficient cleaning.

The cleaner autonomously navigates the classroom, detecting obstacles and adjusting its path for thorough cleaning. Equipped with a powerful motor and fan, it effectively collects dust and debris, leaving the space spotless. Real-time monitoring capabilities identify areas with high moisture levels, allowing for proactive maintenance to prevent mold growth.

Simple push-button controls make operation easy for users of all ages, while customizable settings cater to various classroom environments and preferences. In summary, the CleanTech Smart Cleaner offers a user-friendly, efficient solution for maintaining cleanliness and promoting a healthier learning environment.

## What motivated us:
Our motivation arises from a common problem: the lack of cleanliness and hygiene in our classroom. We believe a clean environment is essential for effective learning. With diverse skills and a shared commitment to improvement, we're driven to develop a smart cleaner using technology. Our goal is to create a healthier space for ourselves and our peers, enhancing the learning experience for all.

## What are we looking forward to:
We are looking forward to implementing our smart cleaner in the classroom, making a tangible difference in maintaining cleanliness and hygiene. Additionally, we anticipate the opportunity to refine and optimize our device based on feedback and real-world usage. Ultimately, we aim to see our cleaner become a standard fixture in classrooms, promoting better hygiene practices and contributing to a healthier learning environment for students and teachers alike.

[Home](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Home.md?ref_type=heads) | [Main Project](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Project.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Team.md?ref_type=heads) | [Vocabulary](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Vocabulary.md?ref_type=heads)|[Bibliography](https://gitlab.com/project-work222511/smart-cleaner/auto-cleaner/-/blob/main/Main/Main/Bibliography.md?ref_type=heads)  | [Progress](https://gitlab.com/-/ide/project/project-work222511/smart-cleaner/auto-cleaner/edit/main/-/)
